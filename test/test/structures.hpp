struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}

    static ListNode* CreateList(int numberOfNodes);
    static void DestroyList(ListNode* top);
    static void ShowList(ListNode* top);
};

class Solution {
    public:
    ListNode* IterationReverseList(ListNode* top);
    ListNode* RecursiveReverseList(ListNode* top);
};