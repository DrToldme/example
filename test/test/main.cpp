#include <iostream>
#include "structures.hpp"

int main() 
{
    ListNode* top = ListNode::CreateList(7);
    Solution s;

    ListNode::ShowList(top);
    top = s.IterationReverseList(top);
    ListNode::ShowList(top);
    top = s.RecursiveReverseList(top);
    ListNode::ShowList(top);
    std::cout << "success" << std::endl;

    ListNode::DestroyList(top);
    system("PAUSE");
    return 0;
}