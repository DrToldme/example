#include <iostream>
#include "structures.hpp"

ListNode* ListNode::CreateList(int numberOfNdoes) {
    ListNode* nodeptr = nullptr;
    for (int i = numberOfNdoes; i >= 1; i--) {
        if (i == numberOfNdoes) {
            nodeptr = new ListNode(i);
            continue;
        }

        ListNode* nextNode = new ListNode(i);
        nextNode->next = nodeptr;
        nodeptr = nextNode;
    }

    return nodeptr;
}

void ListNode::DestroyList(ListNode* top) {
    while (top->next != nullptr) {
        ListNode* currentNode = top;
        top = top->next;
        delete currentNode;
    }

    delete top;
    top = nullptr;
}

void ListNode::ShowList(ListNode* top) {
    ListNode* iterator = top;

    std::cout << "List of values: ";
    while (iterator != nullptr) {
        std::cout << iterator->val << "->";
        iterator = iterator->next;
    }

    std::cout << "NULL" << std::endl;
}

ListNode* Solution::IterationReverseList(ListNode* top) {
    ListNode* prevNode = nullptr;
    ListNode* nextNode = top->next;

    while (nextNode != nullptr) {
        top->next = prevNode;
        prevNode = top;
        top = nextNode;
        nextNode = top->next;
    }

    top->next = prevNode;
    return top;
}

ListNode* Solution::RecursiveReverseList(ListNode* top) {
    if (top->next == nullptr || top == nullptr) {
        return top;
    }
    
    ListNode* nextNode = RecursiveReverseList(top->next);
    top->next->next = top;
    top->next = nullptr;

    return nextNode;
}